const express = require('express')
const app = express()
const testeConnect = require('./db/testeConnect')

class AppController {
    constructor() {
      this.express = express();
      this.middlewares();
      this.routes();
      testeConnect();
    }

    middlewares() {
      this.express.use(express.json());
    }

    routes() {
      const apiRoutes= require('./routes/apiRoutes')
      this.express.use('/', apiRoutes);
    //   this.express.get("/health/", (_, res) => {
    //     res.send({ status: "ok" });
    //   });
    }
  }

  module.exports = new AppController().express;