const testeController = require('../controller/teacherController')
const JSONPlaceholderController = require('..//controller/JSONPlaceholderController')
const dbController = require('../controller/dbController');
const clienteController = require('../controller/clienteController');
const router = require('express').Router()

router.get('/teste', testeController.getTesteController);
router.post('/cadastroaluno',testeController.postAlunoController);
router.put('/cadastroaluno/:id', testeController.putAlunoController); 
router.delete('/cadastroaluno', testeController.deleteAlunoController);

router.get("/external/", JSONPlaceholderController.getUsers)
router.get("/external/io", JSONPlaceholderController.getUsersWebsiteIO)
router.get("/external/com", JSONPlaceholderController.getUsersWebsiteCOM)
router.get("/external/net", JSONPlaceholderController.getUsersWebsiteNET)
router.get("/external/filter", JSONPlaceholderController.getUsersDominio)

router.get("/tablesNames", dbController.getTablesNames)
router.get("/tablesDescription", dbController.getTablesDescriptions)

router.post("/postCliente", clienteController.createCliente)
module.exports = router
