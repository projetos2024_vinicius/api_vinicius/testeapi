const axios = require("axios");
const { get } = require("mongoose");

module.exports = class JSONPlaceholderController{
    static async getUsers(req,res){
        try {
            const response = await axios.get("https://jsonplaceholder.typicode.com/users")
            const users = response.data;
            res.status(200).json({message:'Aqui estao os usuários captados da Api publica JSONPlaceholder', users})
        } catch (error) {
        console.error(error)   
        res.status(500).json({error:"Falha ao encontrar usuarios"})
        }
    }

    static async getUsersWebsiteIO(req, res){
        try {
            const response = await axios.get("https://jsonplaceholder.typicode.com/users")
            const users = response.data.filter(
                (user)=> user.website.endsWith(".io")
                )
            const numIO = users.length

            res.status(200).json({message:'Aqui estao os usuários com dominio IO, O numero de usuarios que usam isso são:', numIO,  users, })
        } catch (error) {
            console.error(error)
            res.status(500).json({error:"Falha ao encontrar usuarios"})
        }
    }
    static async getUsersWebsiteCOM(req, res){
        try {
            const response = await axios.get("https://jsonplaceholder.typicode.com/users")
            const users = response.data.filter(
                (user)=> user.website.endsWith(".com")
                )
            const numCom = users.length

            res.status(200).json({message:'Aqui estao os usuários com dominio Com, O numero de usuarios que usam isso são:', numCom,  users, })
        } catch (error) {
            console.error(error)
            res.status(500).json({error:"Falha ao encontrar usuarios"})
        }
    }
    static async getUsersWebsiteNET(req, res){
        try {
            const response = await axios.get("https://jsonplaceholder.typicode.com/users")
            const users = response.data.filter(
                (user)=> user.website.endsWith(".net")
                )
            const numNet = users.length

            res.status(200).json({message:'Aqui estao os usuários com dominio Net, O numero de usuarios que usam isso são:', numNet,  users, })
        } catch (error) {
            console.error(error)
            res.status(500).json({error:"Falha ao encontrar usuarios"})
        }
    }

    static async getUsersDominio(req, res) {
        try {
            const response = await axios.get("https://jsonplaceholder.typicode.com/users");
            const { dominio } = req.query;
    
            if (!dominio) {
                return res.status(400).json({ error: 'Domínio não especificado na consulta' });
            }
    
            const users = response.data.filter(user => {
                return user.website.endsWith(dominio);
            });
    
            const numUsers = users.length;

            res.status(200).json({
                message: `Aqui estão os usuários com domínio ${dominio}: ${numUsers}`,
            });
        } catch (error) {
            console.error(error);
            res.status(500).json({ error: "Falha ao encontrar usuários" });
        }
    }


};