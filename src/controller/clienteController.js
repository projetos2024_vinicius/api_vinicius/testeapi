const connect = require("../db/connect");

module.exports = class clienteController{
    static async createCliente(req, res){
        const{
            telefone,
            nome,
            cpf,
            logradouro,
            numero,
            complemento,
            bairro,
            cidade,
            estado,
            cep,
            referencia
        } = req.body;

        //verificando se o atributo chave é diferente de 0
        if (telefone !== 0) {
            const query = `insert into cliente (telefone, nome, cpf, logradouro, numero, complemento, bairro, cidade, estado, cep, referencia) values ( 
                '${telefone}',
                '${nome}',
                '${cpf}',
                '${logradouro}',
                '${numero}',
                '${complemento}',
                '${bairro}',
                '${cidade}',
                '${estado}',
                '${cep}',
                '${referencia}'
            )`;0
        try {
            connect.query(query, function(err){
                if(err){
                    console.log(err);
                    res.status(500).json({error:"Usuário não cadastrado no banco!"});
                    return;
                }
                console.log("Inserindo no Banco!");
                res.status(201).json({message:"Usuário criado com sucesso!"});
            });
        } catch (error) {
            console.error("Errp ap execitar o insert! - ", error);
            res.status(500).json({error:"Erro interno no servidor!"})
        }
     }//fim da if
     else{
        res.status(400).json({message:"O Telefone é obrigatório!"});
     }//fim do else
    }//fim do createCliente
    static async getAllClientes(req, res){
        const query = `select * from cliente`;
    }
}//fim do module