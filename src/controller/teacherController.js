module.exports = class testeController {
  static alunos = [];

  static async postAlunoController(req, res) {
    const { nome, idade, profissao, cursoMatriculado } = req.body;
    
    if (!nome || !idade || !profissao || !cursoMatriculado) {
      return res.status(400).json({ mensagem: 'Por favor, forneça todos os campos necessários' });
    }

    const alunoId = Date.now().toString();
    const aluno = { id: alunoId, nome, idade, profissao, cursoMatriculado };
    testeController.alunos.push(aluno);
    
    return res.status(200).json({ message: "Aluno cadastrado", alunoId });
  }
  
  static async deleteAlunoController(req, res) {
    const alunoId = req.query.id;
    if (!alunoId) {
        return res.status(400).json({ mensagem: 'Por favor, forneça o ID do aluno' });
    }

    // Verifica se o aluno com o ID fornecido existe na lista de alunos
    const alunoIndex = testeController.alunos.findIndex(aluno => aluno.id === alunoId);
    if (alunoIndex === -1) {
      return res.status(404).json({ mensagem: 'Aluno não encontrado' });
    }

    return res.status(200).json({ mensagem: `Aluno com ID ${alunoId} deletado com sucesso` });
  }

  static async putAlunoController(req, res) {
    const alunoId = req.params.id;
    const { nome, idade, profissao, cursoMatriculado } = req.body;

    if (!nome || !idade || !profissao || !cursoMatriculado) {
      return res.status(400).json({ mensagem: 'Por favor, forneça todos os campos necessários' });
    }

    const alunoIndex = testeController.alunos.findIndex(aluno => aluno.id === alunoId);
    if (alunoIndex === -1) {
      return res.status(404).json({ mensagem: 'Aluno não encontrado' });
    }

    
    testeController.alunos[alunoIndex] = { id: alunoId, nome, idade, profissao, cursoMatriculado };

    return res.status(200).json({ mensagem: `Aluno atualizado com sucesso`, aluno: testeController.alunos[alunoIndex] });
  }

  static async getTesteController(req, res) {
    res.status(200).json({
      algumaMensagem: {
        algumaMensagem: 'Vitao o MAIOR do senai',
        algumaNúmero: 26,
      },
    });
  }
};
